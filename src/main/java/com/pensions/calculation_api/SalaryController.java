package com.pensions.calculation_api;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pensions.calculation_api.Model.Salary;
import com.pensions.calculation_api.Model.SalaryDTO;
import com.pensions.calculation_api.service.SalaryService;

@CrossOrigin(origins = "*")
@EnableAutoConfiguration
@RestController
@RequestMapping("/salary")
public class SalaryController {

	@Autowired
	public SalaryService service;

	@GetMapping("/getsalary/{code}/{salary}")

	public Salary getSalary(@PathVariable("code") String code, @PathVariable("salary") Double Salary) {
		System.out.println("method");
		return service.getSalary(code, Salary);
	}

	@GetMapping("/getsal/{scale}/{grade}/{circular}/{salary}/{target}/{retirementDate}/{lastIncrementDate}")
	public SalaryDTO getsalary(@PathVariable("scale")String scale,@PathVariable("grade") String grade,@PathVariable("circular") String circular,@PathVariable("salary") double salary,
			@PathVariable("target")String target,@PathVariable("retirementDate")Date retirementDate, @PathVariable("lastIncrementDate") Date lastIncrementDate) {
		return service.getSalaryFor(scale, grade, circular, salary, target, retirementDate, lastIncrementDate);

	}

	@GetMapping("/tt")
	public String msg() {
		System.out.println("test String");
		String s = "test";
		return s;

	}

}

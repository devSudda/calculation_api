package com.pensions.calculation_api.DBConnection;

import java.sql.Connection;
import java.sql.DriverManager;

public enum MySqlDbConnection {

	MANAGER;
	
	public static final String  JDBC_DRIVER = "com.mysql.jdbc.Driver";
	
	public static final String DB_URL_193 = "jdbc:mysql://192.168.100.150:3306/live"; 	//Test
//	public static final String DB_URL_193 = "jdbc:mysql://192.168.100.145:3306/live";	
//	public static final String DB_URL_193 = "jdbc:mysql://192.168.204.193:3306/live";	//RedHat
	
	public static final String USER = "sasini";	//Test
	public static final String PASS = "sasini";	//Test
	
//	public static final String USER = "root";
//	public static final String PASS = "root";
	
//	public static final String USER = "develop";	//RedHat
//	public static final String PASS = "Root@d0p";	//RedHat	
	
	
	public Connection get193Connection() {
		Connection DbConn = null;
		try {
			Class.forName(JDBC_DRIVER);
			DbConn = DriverManager.getConnection(DB_URL_193,USER,PASS);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return DbConn;
		
	}
	
	public void CloseConnection (Connection conn) {
		try {
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

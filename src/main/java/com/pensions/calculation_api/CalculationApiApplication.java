package com.pensions.calculation_api;
/* 20230223 - Dev Sudesh : Init the project */

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EntityScan("com.delivery.domain")
@ComponentScan({ "com.delivery.request" })
@ComponentScan(basePackages = { "com.pensions.calculation_api" })

public class CalculationApiApplication extends  SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(CalculationApiApplication.class, args);
		System.out.println("System on");
	}
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(CalculationApiApplication.class);
	}

}

package com.pensions.calculation_api.Model;

public class SalaryDTO {
	private double firstAllowance;
	private double secondAllowance;
	private int step;
	private Double basicSalary;
	
	
	
	
	public double getFirstAllowance() {
		return firstAllowance;
	}
	public void setFirstAllowance(double firstAllowance) {
		this.firstAllowance = firstAllowance;
	}
	public double getSecondAllowance() {
		return secondAllowance;
	}
	public void setSecondAllowance(double secondAllowance) {
		this.secondAllowance = secondAllowance;
	}
	public int getStep() {
		return step;
	}
	public void setStep(int step) {
		this.step = step;
	}
	public Double getBasicSalary() {
		return basicSalary;
	}
	public void setBasicSalary(Double basicSalary) {
		this.basicSalary = basicSalary;
	}
	
	
}

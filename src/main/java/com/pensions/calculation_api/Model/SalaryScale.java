package com.pensions.calculation_api.Model;

public class SalaryScale {
	private String code;
	private String scale;
	private String grade;
	private String circular;

	public SalaryScale(String scale, String grade, String circular) {
		super();
		this.scale = scale;
		this.grade = grade;
		this.circular = circular;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getScale() {
		return scale;
	}

	public void setScale(String scale) {
		this.scale = scale;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public String getCircular() {
		return circular;
	}

	public void setCircular(String circular) {
		this.circular = circular;
	}

}

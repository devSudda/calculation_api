package com.pensions.calculation_api.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import com.pensions.calculation_api.Model.Salary;
import com.pensions.calculation_api.Model.SalaryDTO;

@Component
public interface SalaryService {

//	public SalaryDTO getSalary();
	public Salary getSalary(String code, double salary );
	public Salary getSalary(String code, int step);
	public Salary getFinalSalaryStep(String code);
	public List<Salary> getSalaries(String code);
	
	public SalaryDTO getSalaryFor(String scale, String grade, String circular, double salary, String target, Date retirementDate, Date lastIncrementDate);
	
	public Salary testSal(String code);
}

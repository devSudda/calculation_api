package com.pensions.calculation_api.controller;

import java.sql.Connection;

import com.pensions.calculation_api.DBConnection.MySqlDbConnection;

public class BaseDao {

	public Connection getConn() {
		return MySqlDbConnection.MANAGER.get193Connection();
	}
	
	public void closeConnection(Connection Conn) {
		
		try {
			MySqlDbConnection.MANAGER.CloseConnection(Conn);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}

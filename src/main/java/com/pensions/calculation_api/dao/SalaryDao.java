package com.pensions.calculation_api.dao;

import java.util.Date;
import java.util.List;

import com.pensions.calculation_api.Model.Salary;
import com.pensions.calculation_api.Model.SalaryDTO;

public interface SalaryDao {
	public Salary getSalary(String code, double salary );
	public Salary getSalary(String code, int step);
	public Salary getFinalSalaryStep(String code);
	public List<Salary> getSalaries(String code);
	public SalaryDTO getSalaryFor(String scale, String grade, String circular, double salary, String target,
			Date retirementDate, Date lastIncrementDate);
	
	public Salary testSal(String code);
	
	
}

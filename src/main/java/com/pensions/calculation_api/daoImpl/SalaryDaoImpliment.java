package com.pensions.calculation_api.daoImpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.junit.platform.commons.PreconditionViolationException;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.deser.std.DateDeserializers.CalendarDeserializer;
import com.pensions.calculation_api.Exceptions.DataNotFoundException;
import com.pensions.calculation_api.Model.Salary;
import com.pensions.calculation_api.Model.SalaryDTO;
import com.pensions.calculation_api.Model.SalaryScale;
import com.pensions.calculation_api.controller.BaseDao;
import com.pensions.calculation_api.dao.SalaryDao;

public class SalaryDaoImpliment extends BaseDao implements SalaryDao {
	@Autowired
	public Salary sal;

	/**
	 * get salary according to code and salary
	 * 
	 * @param code
	 * @param salary
	 *            return salary
	 */

	@Override
	public Salary getSalary(String code, double salary) {

		Connection con = getConn();

		sal = null;

		String sql = "SELECT salary_id,scale,step,basic_salary,gross_salary FROM salary WHERE scale= ? AND basic_salary = ?";
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, code);
			ps.setDouble(2, salary);

			try {
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					sal = new Salary();
					sal.setId(rs.getInt(1));
					sal.setScale(rs.getString(2));
					sal.setStep(rs.getInt(3));
					sal.setBasicSalary(rs.getDouble(4));
					sal.setGrossSalary(rs.getDouble(5));
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return sal;
	}

	/**
	 * get salary according to code and step
	 * 
	 * @param code
	 * @param step
	 *            return
	 */
	@Override
	public Salary getSalary(String code, int step) {

		sal = null;
		Connection con = getConn();
		String sql = "SELECT salary_id,scale,step,basic_salary,gross_salary FROM `salary` WHERE scale= ? AND step = ?";
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, code);
			ps.setDouble(2, step);

			try {
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					sal.setId(rs.getInt(1));
					sal.setScale(rs.getString(2));
					sal.setStep(rs.getInt(3));
					sal.setBasicSalary(rs.getDouble(4));
					sal.setGrossSalary(rs.getDouble(5));

				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return sal;
	}

	@Override
	public Salary getFinalSalaryStep(String code) {

		sal = null;

		Connection con = getConn();
		String sql = "SELECT MAX(step) FROM salary WHERE scale = ? ";
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, code);
//			ps.setInt(arg0, arg1);//for test

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				sal.setStep(rs.getInt(1));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return sal;
	}

	@Override
	public List<Salary> getSalaries(String code) {

		return null;
	}

	/**
	 * get salary dto
	 */
	@Override
	public SalaryDTO getSalaryFor(String scale, String grade, String circular, double salary, String target,
			Date retirementDate, Date lastIncrementDate) {

		SalaryScale scaleModel = new SalaryScale(circular, grade, scale);
		Salary firstModel = getSalary(scaleModel.getCode(), salary);

		if (firstModel == null) {

			firstModel = getFinalSalaryStep(scaleModel.getCode());

			if (firstModel.getBasicSalary() > salary) {
				throw new DataNotFoundException("salary you provided seems to be invalid");

			} else {

				SalaryDTO finalModel = new SalaryDTO();
				Salary PrviousSalary = getSalary(scaleModel.getCode(), firstModel.getStep());

				Double SalaryGap = firstModel.getBasicSalary() - PrviousSalary.getBasicSalary();

				if ((salary - firstModel.getBasicSalary()) % SalaryGap != 0)				
				throw new DataNotFoundException("Salary you provided seems to be invalid");

				// calculate first incerment
				// finalModel.setFirstAllowance(getincrementAllowance(SalaryGap, retirementDate,
				// lastIncrementDate, circular));
				// finalModel.setFirstAllowance(getincrementAllowance(SalaryGap, retirementDate,
				// lastIncrementDate, circular));

				// int incrementCount = (int) ((int) salary - firstModel.getBasicSalary() /
				// SalaryGap);
				// scaleModel.setCircular(target);
				// firstModel = getFinalSalaryStep(scaleModel.getCode());

				// PrviousSalary = getSalary(scaleModel.getCode(), firstModel.getStep() - 1);

				// SalaryGap = firstModel.getBasicSalary() - PrviousSalary.getBasicSalary();
				// finalModel.setSecondAllowance(getincrementAllowance(SalaryGap,
				// retirementDate, lastIncrementDate, circular));

				// finalModel.setBasicSalary(firstModel.getBasicSalary()+
				// (incrementCount*SalaryGap));
				// finalModel.setStep(firstModel.getStep() + incrementCount);

				// return finalModel;

			}

		}

		// Calculating the increment;

		SalaryDTO finalModel = new SalaryDTO();

		Salary SecondModel = getSalary(scaleModel.getCode(),
				(getPreviousStep(firstModel.getStep(), firstModel.getScale())));

		finalModel.setFirstAllowance(getincrementAllowance(firstModel.getBasicSalary() - SecondModel.getBasicSalary(),
				retirementDate, lastIncrementDate, circular));

		// requested circuler target
		scaleModel.setCircular(target);
		SecondModel = getSalary(scaleModel.getCode(), firstModel.getStep());
		firstModel = getSalary(scaleModel.getCode(), getPreviousStep(firstModel.getStep(), firstModel.getScale()));

		if (SecondModel == null) {
			throw new DataNotFoundException(
					"Something went wrong while tring to calculate the salary details for 2020. please contact system admin");
		}

		finalModel.setSecondAllowance(getincrementAllowance(SecondModel.getBasicSalary() - firstModel.getBasicSalary(),
				retirementDate, lastIncrementDate, circular));
		finalModel.setBasicSalary(SecondModel.getBasicSalary());

		return finalModel;

	}

	// getPrevious step

	private int getPreviousStep(int currentstep, String code) {

		int finalstep = getFinalSalaryStep(code).getStep();
		if (currentstep == finalstep) {
			return finalstep - 1;
		} else {
			return finalstep + 1;
		}

	}

	private double getincrementAllowance(double gap, Date retirementDate, Date lastIncrementDate, String circular) {

		double devider = 365.0;

		GregorianCalendar calender = new GregorianCalendar();

		calender.setTime(retirementDate);
		int retirementYear = calender.get(calender.YEAR);
		int month = calender.get(calender.MONTH);
		int day = calender.get(calender.DAY_OF_MONTH);
		boolean leapYear = false;
		if (Double.valueOf(retirementYear) % 4 == 0) {
			leapYear = true;
			devider = 366.0;
		}

		if (gap < 0)
			gap = -gap;
		Double amoutPerDay = (gap * 12) / devider;

		int daysInBetween = (int) ((retirementDate.getTime() - lastIncrementDate.getTime()) / (1000 * 60 * 60 * 24));
		if (leapYear) {
			if (daysInBetween > 366) {
				daysInBetween = 0;
			} else if (daysInBetween == 366) {
				daysInBetween = 365;
			} else {
				if (daysInBetween > 365) {
					daysInBetween = 0;
				} else if (daysInBetween == 365) {
					daysInBetween = 364;
				}
			}

		}
		return amoutPerDay * daysInBetween;

	}

	@Override
	public Salary testSal(String code) {

		sal = null;
		Connection con = getConn();
		String sql = "SELECT salary_id,scale,step,basic_salary,gross_salary FROM salary WHERE scale = ?";
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, code);
			// ps.setDouble(2, step);

			try {
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					sal.setId(rs.getInt(1));
					sal.setScale(rs.getString(2));
					sal.setStep(rs.getInt(3));
					sal.setBasicSalary(rs.getDouble(4));
					sal.setGrossSalary(rs.getDouble(5));

				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return sal;
	}

}

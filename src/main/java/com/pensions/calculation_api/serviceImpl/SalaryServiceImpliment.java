package com.pensions.calculation_api.serviceImpl;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.pensions.calculation_api.Model.Salary;
import com.pensions.calculation_api.Model.SalaryDTO;
import com.pensions.calculation_api.controller.DaoManager;
import com.pensions.calculation_api.service.SalaryService;

@Service
public class SalaryServiceImpliment implements SalaryService {

	@Override
	public Salary getSalary(String code, double salary) {
		
		return DaoManager.saldao().getSalary(code, salary);
	}

	@Override
	public Salary getSalary(String code, int step) {
		
		return DaoManager.saldao().getSalary(code, step);
	}

	@Override
	public Salary getFinalSalaryStep(String code) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Salary> getSalaries(String code) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SalaryDTO getSalaryFor(String scale, String grade, String circular, double salary, String target,
			Date retirementDate, Date lastIncrementDate) {
		
		return DaoManager.saldao().getSalaryFor(scale, grade, circular, salary, target, retirementDate, lastIncrementDate);
				
	}

	@Override
	public Salary testSal(String code) {
		
		return DaoManager.saldao().testSal(code);
	}

}

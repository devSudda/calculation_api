package com.pensions.calculation_api.Exceptions;



import org.springframework.boot.web.server.WebServerException;

public class DataNotFoundException extends WebServerException  {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7691090690420940905L;

	public DataNotFoundException(String message) {
		super(message, null);
		
	}

	/**
	 * 
	 */
	

}
